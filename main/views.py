from django.shortcuts import render


def home(request):
    return render(request, 'home.html')

def storyempat(request):
    return render(request, 'storytiga.html')

def edu(request):
    return render(request, 'storytiga/edu.html')

def exp(request):
    return render(request, 'storytiga/exp.html')

def gallery(request):
    return render(request, 'storytiga/gallery.html')

def storysatu(request):
    return render(request, 'storysatu.html')

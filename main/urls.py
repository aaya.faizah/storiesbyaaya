from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('storytiga', views.storyempat, name='storytiga'),
    path('storytiga/edu', views.edu, name='edu'),
    path('storytiga/exp', views.exp, name='exp'),
    path('storytiga/gallery', views.gallery, name='gallery'),
    path('storysatu', views.storysatu, name='storysatu'),
]

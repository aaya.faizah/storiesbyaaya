# Generated by Django 3.1.2 on 2020-10-16 11:30

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='FormMataKuliah',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('namamatkul', models.CharField(max_length=50)),
                ('namadosen', models.CharField(max_length=50)),
                ('sks', models.IntegerField()),
                ('deksripsi', models.CharField(max_length=240)),
                ('semester', models.CharField(max_length=240)),
            ],
        ),
    ]

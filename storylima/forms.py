from django import forms
from .models import MataKuliah

class FormMataKuliah(forms.Form):
    Nama_Matkul = forms.CharField()
    Nama_Dosen = forms.CharField()
    SKS = forms.IntegerField()
    Deskripsi = forms.CharField()
    Semester = forms.CharField()

    
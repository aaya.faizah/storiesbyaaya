from django.shortcuts import render, redirect, HttpResponseRedirect
from .models import MataKuliah
from .forms import FormMataKuliah
# Create your views here.

def homelima(request):
    return render(request, 'storylima.html')

def listmatkul(request):
    mataKuliah = MataKuliah.objects.all()
    response = {
        'matakuliahs' : mataKuliah
    }
    return render(request, 'listmatkul.html', response)

def formmatkul(request):
    regist = FormMataKuliah()
    if request.method == 'POST':
        regist = FormMataKuliah(request.POST)

        MataKuliah.objects.create(
            Nama_Matkul = request.POST['Nama_Matkul'],
            Nama_Dosen = request.POST['Nama_Dosen'],
            SKS = request.POST['SKS'],
            Deskripsi = request.POST['Deskripsi'],
            Semester = request.POST['Semester'],
        )

        return HttpResponseRedirect('/storylima/listmatkul')

    response = {
        'regist' : regist
    }

    return render(request, 'formmatkul.html', response)

def hapusmatkul(request, id_matkul):
    try:
        MataKuliah.objects.filter(id=id_matkul).delete()
        return redirect('storylima:listmatkul')
    except:
        return redirect('storylima:listmatkul')

def detailmatkul(request, id_matkul):
    detail = MataKuliah.objects.filter(id=id_matkul).get(id=id_matkul)
    response = {
        'detailmatkul' : detail
    }
    return render(request, 'detailmatkul.html', response)
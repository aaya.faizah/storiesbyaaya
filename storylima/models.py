from django.db import models

# Create your models here.
class MataKuliah(models.Model):
    Nama_Matkul = models.CharField(max_length=50)
    Nama_Dosen = models.CharField(max_length=50)
    SKS = models.IntegerField()
    Deskripsi = models.CharField(max_length=240)
    Semester = models.CharField(max_length=240)

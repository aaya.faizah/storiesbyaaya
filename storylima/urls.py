from django.urls import path
from .views import listmatkul, formmatkul, homelima, hapusmatkul, detailmatkul

app_name = 'storylima'

urlpatterns = [
    path('', homelima),
    path('listmatkul', listmatkul, name='listmatkul'),
    path('formmatkul', formmatkul, name='formmatkul'),
    path('hapus/P<int:id_matkul>', hapusmatkul, name='hapus'),
    path('detail/P<int:id_matkul>/', detailmatkul, name='detail'),
]

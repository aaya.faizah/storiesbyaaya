from django import forms
from .models import Activity, Person

class ActivityForm(forms.Form):
    activity_name = forms.CharField()

class PersonForm(forms.Form):
    person_name = forms.CharField()

    
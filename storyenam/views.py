from django.shortcuts import render, redirect, HttpResponseRedirect
from .models import Activity, Person
from .forms import ActivityForm, PersonForm

# Create your views here.


def storyenam(request):
    return render(request, 'storyenam.html')


def listactivity(request):
    activity = Activity.objects.all()
    person = Person.objects.all()
    response = {
        'activity': activity,
        'person': person
    }

    return render(request, 'list.html', response)


def registeractivity(request):
    registeractivity = ActivityForm(request.POST or None)
    
    if registeractivity.is_valid():
        if request.method == 'POST':
            Activity.objects.create(
                activity_name = registeractivity.cleaned_data.get('activity_name')
            )
            return HttpResponseRedirect("/storyenam/list")

    response = {
        'registeractivity':registeractivity
    }

    return render(request, 'registeractivity.html', response)


def registerperson(request, id_person):
    registerperson = PersonForm(request.POST or None)

    if registerperson.is_valid():
        if request.method == 'POST':
            saveperson = registerperson.cleaned_data
            registered = Person()
            registered.person_name = saveperson['person_name']
            registered.activity = Activity.objects.get(id=id_person)
            registered.save()
            return redirect('/storyenam/list')

    response = {
        'registerperson': registerperson
    }

    return render(request, 'registerperson.html', response)


def deleteperson(request, id_person):
    try:
        Person.objects.filter(id=id_person).delete()
        return redirect('storyenam:list')
    except:
        return redirect('storyenam:list')


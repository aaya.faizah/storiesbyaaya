from django.test import TestCase, Client
from django.apps import apps
from django.urls import resolve, reverse

from .models import Activity, Person
from .views import storyenam, registeractivity, registerperson, listactivity, deleteperson
from .forms import ActivityForm, PersonForm
from .apps import StoryEnamConfig

#Create your tests here.
class TestApp(TestCase):
    def test_app(self):
        self.assertEqual(StoryEnamConfig.name, 'storyenam')
        self.assertEqual(apps.get_app_config('storyenam').name, 'storyenam')

class TestingModels(TestCase):
    def setUp(self):
        self.activity = Activity.objects.create(
            activity_name = "nulis"
        )

        self.person = Person.objects.create(
            person_name = "aaya"
        )

    def test_instances(self):
        self.assertEqual(Activity.objects.count(), 1)
        self.assertEqual(Person.objects.count(), 1)

class TestingForms(TestCase):
    def test_form_valid(self):
        activity_form = ActivityForm(
            data={
                "activity_name":"nulis"
            }
        )
        self.assertTrue(activity_form.is_valid())

        person_form = PersonForm(
            data={
                "person_name":"aaya"
            }
        )
        self.assertTrue(person_form.is_valid())
    
    def test_form_invalid(self):
        activity_form = ActivityForm(
            data={}
        )
        self.assertFalse(activity_form.is_valid())

        person_form = PersonForm(
            data={}
        )
        self.assertFalse(activity_form.is_valid())

class TestingViews(TestCase):
    def setUp(self):
        self.client = Client()

    def test_storyenam(self):
        response = self.client.get("/storyenam/")
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "storyenam.html")

    def test_listactivity_get(self):
        response = self.client.get("/storyenam/list")
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "list.html")

    def test_registeractivity_get(self):
        response = self.client.get("/storyenam/registeractivity")
        self.assertTemplateUsed(response, "registeractivity.html")
        self.assertEqual(response.status_code, 200)
    
    def test_registeractivity_post_valid(self):
        response = self.client.post("/storyenam/registeractivity",
            {
                "activity_name":"nulis",
            },
            follow=True
        )
        self.assertEqual(response.status_code, 200)

    def test_registerperson_get(self):
        registeredact = Activity(activity_name="nulis")
        registeredact.save()
        response = self.client.get("/storyenam/registerperson/P1")
        self.assertTemplateUsed(response, "registerperson.html")
        self.assertEqual(response.status_code, 200)
    
    def test_registerperson_post(self):
        registeredact = Activity(activity_name="nulis")
        registeredact.save()
        response = self.client.post("/storyenam/registerperson/P1", 
            data={}
            )
        self.assertTemplateUsed(response, 'registerperson.html')
        self.assertEqual(response.status_code, 200)

    #failed
    # def test_delete(self):
    #     activity = Activity(
    #         activity_name="nulis"
    #     )
    #     activity.save()

    #     person = Person(
    #         person_name="aaya",
    #         activity=Activity.objects.get(pk=1)
    #     )
    #     person.save()

    #     response = self.client.get('/storyenam/deleteperson', args=[person.pk])
    #     self.assertEqual(Person.objects.count(), 1)
    #     self.assertEqual(response.status_code, 404)

class TestingUrls(TestCase):
    def setUp(self):
        self.client = Client()
        
    def test_main_url(self):
        response = Client().get('')
        self.assertEquals(response.status_code, 200)

    def test_register_activity_url(self):
        response = Client().get('/storyenam/registeractivity')
        self.assertEquals(response.status_code, 200)

    def test_register_name_url(self):
        response = Client().get('/storyenam/registerperson/P1')
        self.assertEquals(response.status_code, 200)

    def test_list_url(self):
        response = Client().get('/storyenam/list')
        self.assertEquals(response.status_code, 200)
    
    def test_list_function(self):
        found = resolve('/storyenam/list')
        self.assertEqual(found.func, listactivity)

    def test_form_function(self):
        found = resolve('/storyenam/registeractivity')
        self.assertEqual(found.func, registeractivity)

class TestingHTML(TestCase):
    def setUp(self):
        self.client = Client()

    def test_homepage_available(self):
        response = Client().get('/storyenam/')
        self.assertTemplateUsed(response, 'storyenam.html')

    def test_registact_available(self):
        response = Client().get('/storyenam/registeractivity')
        self.assertTemplateUsed(response, 'registeractivity.html')

    def test_listact_available(self):
        response = Client().get('/storyenam/list')
        self.assertTemplateUsed(response, 'list.html')


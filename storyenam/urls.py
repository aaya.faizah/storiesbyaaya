from django.urls import path
from django.contrib import admin

from .views import storyenam, listactivity, registeractivity, registerperson, deleteperson

app_name = 'storyenam'

urlpatterns = [
    path('', storyenam, name='storyenam'),
    path('list', listactivity, name='list'),
    path('registeractivity', registeractivity, name='registeractivity'),
    path('registerperson/P<int:id_person>', registerperson, name='registerperson'),
    path('deleteperson/P<int:id_person>', deleteperson, name='deleteperson'),
]
